from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import (
    InvoiceCancelAPI,
    PaymentTransactionsReadonlyViewSet,
    TransactionReversalAPI,
    VoucherCancelAPI,
)

router = routers.SimpleRouter()
router.register(
    r"payment-transactions", PaymentTransactionsReadonlyViewSet, "payment-transactions"
)

urlpatterns = [
    path("reverse-transaction/", TransactionReversalAPI.as_view()),
    path("cancel-invoice/", InvoiceCancelAPI.as_view()),
    path("cancel-voucher/", VoucherCancelAPI.as_view()),
    path("", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
