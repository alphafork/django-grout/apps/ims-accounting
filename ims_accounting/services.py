from .models.records import Invoice, Voucher
from .models.transactions import Account, Ledger, Transaction


def save_ledger_entries(data, record_type, transaction):
    Ledger.objects.filter(transaction=transaction).delete()
    payment_splitup = data.pop("payment_splitup")
    account = data.pop("account")
    if record_type == "invoice":
        record_creation_entry = "Cr"
        payment_transaction_entry = "Dr"
        amount = data["invoice_amount"]
    elif record_type == "voucher":
        record_creation_entry = "Dr"
        payment_transaction_entry = "Cr"
        amount = data["voucher_amount"]
    else:
        raise ValueError('Value is not "invoice" or "voucher"')

    Ledger.objects.create(
        transaction=transaction,
        account=account,
        entry=record_creation_entry,
        amount=amount,
    )
    for paid_account in payment_splitup.keys():
        Ledger.objects.create(
            transaction=transaction,
            account=Account.objects.get(name=paid_account),
            entry=payment_transaction_entry,
            amount=payment_splitup[paid_account],
        )


def save_transaction(data, record_type, transaction_id=None):
    transaction = Transaction.objects.update_or_create(
        id=transaction_id,
        defaults={
            "date": data.get("date"),
            "title": data.get("title"),
            "description": data.get("description"),
        },
    )[0]
    save_ledger_entries(data, record_type, transaction)
    return transaction


def generate_record_no(date, record_type):
    if record_type == "invoice":
        count = Invoice.objects.filter(date=date).count()
        prefix = "I"
    elif record_type == "voucher":
        count = Voucher.objects.filter(date=date).count()
        prefix = "V"
    else:
        raise ValueError('Value is not "invoice" or "voucher"')
    record_no = f"{prefix}{date.strftime('%d%m%y')}{count}"
    return record_no
