from ims_base.serializers import BaseModelSerializer
from rest_framework import serializers

from .models.records import Invoice, Voucher
from .models.transactions import (
    Account,
    ExpenseAccount,
    IncomeAccount,
    Ledger,
    Transaction,
)
from .services import generate_record_no


class PaymentTransactingAccountSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        excluded_fields = [
            "account_category",
        ] + BaseModelSerializer.Meta.excluded_fields


class RecordSerializer(BaseModelSerializer):
    payment_splitup = serializers.JSONField(write_only=True)

    class Meta(BaseModelSerializer.Meta):
        excluded_fields = [
            "transaction",
            "is_cancelled",
        ] + BaseModelSerializer.Meta.excluded_fields

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        amount = validated_data["amount"]
        payment_splitup = validated_data["payment_splitup"]
        array_sum = 0
        validation_errors = {}

        if self.instance and not self.instance.is_edit_allowed(validated_data):
            raise serializers.ValidationError(
                "You are not allowed to update records. Please cancel the record and"
                " generate a new one instead."
            )

        if not int(amount):
            validation_errors.update({"amount": "This field can not be empty."})

        for payment in payment_splitup:
            array_sum += int(payment["amount"])

        if array_sum != int(amount):
            validation_errors.update(
                {"payment_splitup": ("Payment splitup sum and amount should be equal.")}
            )

        if validation_errors:
            raise serializers.ValidationError(validation_errors)
        return validated_data

    def update(self, instance, validated_data):
        validated_data.pop("payment_splitup")
        validated_data.pop("account")
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["payment_splitup"] = instance.transaction.get_payment_splitup()
        data["transaction_id"] = instance.transaction.id
        data["is_cancelled"] = instance.is_cancelled
        return data


class InvoiceSerializer(RecordSerializer):
    account = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=IncomeAccount.objects.all()
    )

    class Meta(RecordSerializer.Meta):
        excluded_fields = [
            "invoice_no",
        ] + RecordSerializer.Meta.excluded_fields
        extra_meta = {
            "account": {
                "related_model_url": "/accounting/income-account",
            }
        }

    def create(self, validated_data):
        payment_splitup = validated_data.pop("payment_splitup")
        account = validated_data.pop("account")
        amount = validated_data.get("amount")
        for payment in payment_splitup:
            payment["account"] = Account.objects.get(name=payment["account"])
            payment["entry"] = "Dr"
        accounts = payment_splitup + [
            {"account": account, "entry": "Cr", "amount": amount}
        ]
        transaction = Transaction()
        validated_data["transaction"] = transaction.save_transaction_with_ledgers(
            title=validated_data.get("title"),
            accounts=accounts,
        )
        validated_data["invoice_no"] = generate_record_no(
            validated_data.get("date"), "invoice"
        )
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["invoice_no"] = instance.invoice_no
        return data


class VoucherSerializer(RecordSerializer):
    account = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=ExpenseAccount.objects.all()
    )

    class Meta(RecordSerializer.Meta):
        excluded_fields = [
            "voucher_no",
        ] + RecordSerializer.Meta.excluded_fields
        extra_meta = {
            "account": {
                "related_model_url": "/accounting/expense-account",
            }
        }

    def create(self, validated_data):
        payment_splitup = validated_data.pop("payment_splitup")
        account = validated_data.pop("account")
        amount = validated_data.get("amount")
        for payment in payment_splitup:
            payment["account"] = Account.objects.get(name=payment["account"])
            payment["entry"] = "Cr"
        accounts = payment_splitup + [
            {"account": account, "entry": "Dr", "amount": amount}
        ]
        transaction = Transaction()
        validated_data["transaction"] = transaction.save_transaction_with_ledgers(
            title=validated_data.get("title"),
            accounts=accounts,
        )
        validated_data["voucher_no"] = generate_record_no(
            validated_data.get("date"), "voucher"
        )
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["voucher_no"] = instance.voucher_no
        return data


class PaymentTransactionsReadonlySerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        model = Ledger

    def to_representation(self, instance):
        data = super().to_representation(instance)
        transaction = instance.transaction
        data["title"] = transaction.title
        data["account_name"] = instance.account.name
        data["date"] = transaction.date
        data["remarks"] = transaction.remarks
        data["invoice"] = ""
        invoice = Invoice.objects.filter(transaction=transaction)
        if invoice:
            data["invoice"] = invoice.get().id
        data["voucher"] = ""
        voucher = Voucher.objects.filter(transaction=transaction)
        if voucher:
            data["voucher"] = voucher.get().id
        return data


class TransactionReversalSerializer(serializers.Serializer):
    transaction = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=Transaction.objects.all()
    )

    class Meta(BaseModelSerializer.Meta):
        fields = ["transaction"]

    def save(self):
        transaction = self.validated_data.pop("transaction")
        transaction.reverse_transaction()
        return transaction


class InvoiceCancelSerializer(serializers.Serializer):
    invoice = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=Invoice.objects.filter(is_cancelled=False)
    )

    class Meta(BaseModelSerializer.Meta):
        fields = ["invoice"]

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if validated_data["invoice"].is_cancelled:
            raise serializers.ValidationError(
                {"invoice": "You can not cancel already cancelled record."}
            )
        return validated_data

    def save(self):
        invoice = self.validated_data.pop("invoice")
        invoice.is_cancelled = True
        invoice.save()
        return invoice


class VoucherCancelSerializer(serializers.Serializer):
    voucher = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=Voucher.objects.filter(is_cancelled=False)
    )

    class Meta(BaseModelSerializer.Meta):
        fields = ["voucher"]

    def save(self):
        voucher = self.validated_data.pop("voucher")
        voucher.is_cancelled = True
        voucher.save()
        return voucher
