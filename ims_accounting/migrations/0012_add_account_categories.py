from django.db import migrations


def add_account_categories(apps, schema_editor):
    AccountType = apps.get_model("ims_accounting", "accounttype")
    AccountCategory = apps.get_model("ims_accounting", "accountcategory")
    asset = AccountType.objects.get(name="asset")
    liability = AccountType.objects.get(name="liability")
    income = AccountType.objects.get(name="income")
    expense = AccountType.objects.get(name="expense")
    equity = AccountType.objects.get(name="equity")
    AccountCategory.objects.get_or_create(name="payment account", account_type=asset)
    AccountCategory.objects.get_or_create(
        name="accounts receivable", account_type=asset
    )
    AccountCategory.objects.get_or_create(
        name="accounts payable", account_type=liability
    )
    AccountCategory.objects.get_or_create(
        name="unearned revenue", account_type=liability
    )
    AccountCategory.objects.get_or_create(name="income", account_type=income)
    AccountCategory.objects.get_or_create(name="expense", account_type=expense)
    AccountCategory.objects.get_or_create(name="equity", account_type=equity)


class Migration(migrations.Migration):
    dependencies = [
        ("ims_accounting", "0011_record_transaction_one_to_one"),
    ]

    operations = [
        migrations.RunPython(
            add_account_categories,
            reverse_code=migrations.RunPython.noop,
        ),
    ]
