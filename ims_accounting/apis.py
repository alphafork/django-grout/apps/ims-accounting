import datetime

from ims_base.apis import BaseAPIView, BaseAPIViewSet
from rest_framework.generics import CreateAPIView
from rest_framework.viewsets import ReadOnlyModelViewSet

from ims_accounting.models.transactions import Ledger, PaymentTransactingAccount

from .serializers import (
    InvoiceCancelSerializer,
    InvoiceSerializer,
    PaymentTransactionsReadonlySerializer,
    TransactionReversalSerializer,
    VoucherCancelSerializer,
    VoucherSerializer,
)


class PaymentTransactionsReadonlyViewSet(BaseAPIView, ReadOnlyModelViewSet):
    serializer_class = PaymentTransactionsReadonlySerializer
    queryset = Ledger.objects.all()

    def get_queryset(self):
        accounts = PaymentTransactingAccount.objects.values_list("id", flat=True)
        filter_map = {"account__in": accounts}
        if self.request.user.groups.filter(name="Accountant").exists():
            yesterday = datetime.date.today() - datetime.timedelta(1)
            filter_map.update({"transaction__date__gte": yesterday})
        return super().get_queryset().filter(**filter_map).order_by("-created_at")


class InvoiceAPI(BaseAPIViewSet):
    serializer_class = InvoiceSerializer

    def get_queryset(self):
        filter_map = {}
        if self.request.user.groups.filter(name="Accountant").exists():
            yesterday = datetime.date.today() - datetime.timedelta(1)
            filter_map = {"transaction__date__gte": yesterday}
        return super().get_queryset().filter(**filter_map).order_by("-updated_at")


class VoucherAPI(BaseAPIViewSet):
    serializer_class = VoucherSerializer

    def get_queryset(self):
        filter_map = {}
        if self.request.user.groups.filter(name="Accountant").exists():
            yesterday = datetime.date.today() - datetime.timedelta(1)
            filter_map = {"transaction__date__gte": yesterday}
        return super().get_queryset().filter(**filter_map).order_by("-updated_at")


class TransactionReversalAPI(CreateAPIView, BaseAPIView):
    serializer_class = TransactionReversalSerializer


class InvoiceCancelAPI(CreateAPIView, BaseAPIView):
    serializer_class = InvoiceCancelSerializer


class VoucherCancelAPI(CreateAPIView, BaseAPIView):
    serializer_class = VoucherCancelSerializer
