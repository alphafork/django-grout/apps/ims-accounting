from datetime import date

from django.contrib.auth.models import User
from django.db import models
from django.db.models.base import pre_save
from django.dispatch import receiver
from ims_base.models import AbstractBaseDetail, AbstractLog


class Transaction(AbstractLog):
    title = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    date = models.DateTimeField(auto_now=True)
    remarks = models.TextField(blank=True)
    is_open = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def save_transaction_with_ledgers(
        self,
        title,
        accounts,
        description=None,
        remarks=None,
    ):
        self.title = title
        self.description = description
        self.remarks = remarks or ""
        self.save()
        Ledger.objects.filter(transaction=self).delete()
        for account in accounts:
            account["transaction"] = self
            Ledger.objects.create(**account)
        return self

    def get_payment_splitup(self):
        payment_splitup = self.ledger_set.filter(
            account__account_category__name="payment account"
        ).values("account__name", "amount")
        for payment in payment_splitup:
            payment.update({"account": payment.pop("account__name")})
        return list(payment_splitup)

    def validate_payment_splitup(self, payment_splitup):
        payment_splitup = payment_splitup.copy()
        for payment in self.get_payment_splitup():
            try:
                payment_splitup.remove(payment)
            except ValueError:
                return False
        if payment_splitup:
            return False
        return True

    def reverse_transaction(self):
        ledger_entries = self.ledger_set.all()
        reversed_transaction = Transaction.objects.create(
            title=f"Reversal of {self.title}",
            date=date.today(),
            remarks=f"Reversal transaction TID {self.id}",
        )
        for ledger in ledger_entries:
            Ledger.objects.create(
                transaction=reversed_transaction,
                account=ledger.account,
                entry="Cr" if ledger.entry == "Dr" else "Dr",
                amount=ledger.amount,
            )
        return reversed_transaction


class AccountType(AbstractBaseDetail):
    pass


class AccountCategory(AbstractBaseDetail):
    account_type = models.ForeignKey(AccountType, on_delete=models.CASCADE)


class AccountTags(AbstractBaseDetail):
    pass


class Account(AbstractBaseDetail):
    account_category = models.ForeignKey(AccountCategory, on_delete=models.CASCADE)
    tags = models.ManyToManyField(AccountTags, blank=True)


class UserAccount(AbstractLog):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    start_date = models.DateField(auto_now_add=True)


class Ledger(AbstractLog):
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    entry = models.CharField(max_length=2, choices=[("Cr", "Credit"), ("Dr", "Debit")])
    amount = models.IntegerField()


class PaymentTransactingAccountManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(account_category__name="payment account")


class PaymentTransactingAccount(Account):
    objects = PaymentTransactingAccountManager()

    class Meta:
        proxy = True


class IncomeAccountManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(account_category__name="income")


class IncomeAccount(Account):
    objects = IncomeAccountManager()

    class Meta:
        proxy = True


class ExpenseAccountManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(account_category__name="expense")


class ExpenseAccount(Account):
    objects = ExpenseAccountManager()

    class Meta:
        proxy = True


@receiver(pre_save, sender=PaymentTransactingAccount)
def add_payment_transacting_account_category(instance, **kwargs):
    account_category = AccountCategory.objects.get(name="payment account")
    instance.account_category = account_category
