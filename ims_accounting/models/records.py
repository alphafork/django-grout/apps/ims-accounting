from datetime import date

from django.db import models
from ims_base.models import AbstractBaseDetail, AbstractLog

from .transactions import Transaction


class VoucherType(AbstractBaseDetail):
    pass


class Record(AbstractLog):
    class Meta:
        abstract = True

    title = models.CharField(max_length=255)
    amount = models.PositiveIntegerField()
    description = models.TextField(blank=True)
    address = models.TextField()
    date = models.DateField()
    remarks = models.TextField(blank=True)
    transaction = models.OneToOneField(
        Transaction, on_delete=models.CASCADE, null=True, blank=True
    )
    is_cancelled = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.is_cancelled:
            self.transaction.reverse_transaction()

    def is_edit_allowed(self, record):
        if self.is_cancelled:
            return False
        return all(
            [
                self.title == record["title"],
                self.amount == record["amount"],
                self.description == record["description"],
                self.address == record["address"],
                self.date == date.today(),
                self.transaction.validate_payment_splitup(record["payment_splitup"]),
            ]
        )


class Invoice(Record):
    invoice_no = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.invoice_no


class Voucher(Record):
    voucher_no = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.voucher_no
