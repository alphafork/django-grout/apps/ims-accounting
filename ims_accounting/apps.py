from django.apps import AppConfig


class IMSAccountingConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_accounting"

    model_strings = {
        "LEDGER": "Ledger",
        "TRANSACTION": "Transaction",
        "ACCOUNT": "Account",
        "USER_ACCOUNT": "UserAccount",
        "PAYMENT_TRANSACTING_ACCOUNT": "PaymentTransactingAccount",
        "ACCOUNT_TYPE": "AccountType",
        "ACCOUNT_CATEGORY": "AccountCategory",
        "ACCOUNT_TAGS": "AccountTags",
        "INVOICE": "Invoice",
        "VOUCHER": "Voucher",
        "VOUCHER_STATUS": "VoucherStatus",
    }
